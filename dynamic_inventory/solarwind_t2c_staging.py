#!/usr/bin/env python
'''
netenegeu dynamic inventory to pull t2c devices from Solarwinds DB
'''
import json
import sys
import argparse
import requests
from credPass import credPass
from requests.packages.urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

class DynamicInventory(object):
    '''
    Gear for API call, output handling and JSON return
    '''

    def __init__(self):

        self.username = credPass().load('awx1.neeaut.netams1.netsys.tmcs', 'username')
        self.password = credPass().load('awx1.neeaut.netams1.netsys.tmcs', 'password')
        self.auth = requests.auth.HTTPBasicAuth(self.username, self.password)
        self.url = """https://10.226.234.117:17778/SolarWinds/InformationService/v3/Json/Query?query=SELECT+n.CustomProperties.Country+,+n.CustomProperties.City+,+n.Caption+,+n.IP_Address+,+n.IOSVersion+,+n.MachineType+FROM+Orion.Nodes n+WHERE+n.CustomProperties.Device_Type='t2c= Tier2c'AND+n.Status+=1"""
        self.headers = {'Content-Type': 'application/json'}
        self.inventory = {}
        self.read_cli_args()

        # Called with `--list`.
        if self.args.list:
            self.inventory = self.solarwind_inventory()
        # Called with `--host [hostname]`.
        elif self.args.host:
            # Not implemented, since we return _meta info `--list`.
            self.inventory = self.empty_inventory()
        # If no groups or vars are present, return an empty inventory.
        else:
            self.inventory = self.empty_inventory()

        print json.dumps(self.inventory)

    def solarwind_inventory(self):
        '''
        Solarwinds API call and JSON return
        '''
        response = requests.get(self.url, verify=False, auth=self.auth, headers=self.headers)
        status_code = str(response.status_code)

        if '400' in status_code:
            print '400, Bad Request. Make sure you passed the right URL'
            sys.exit(1)

        elif '401' in status_code:
            print '401, Authentication Error. Please verify Username and/or Password'
            sys.exit(1)

        elif '403' in status_code:
            print '''
            403, Access Forbidden.
            Make sure you have the right permission to access to the URL required
            '''
            sys.exit(1)

        elif '404' in status_code:
            print '404, Page Not Found. What you are looking for it doesn\'t exists'
            sys.exit(1)

        elif '200' in status_code:
            api_output = json.loads(response.text).values()

            host_list = list()
            host_var = dict()
            host_staging = [
            'be-wereldhave.netops.tmcs',
            'de-kaiserslauternfc.netops.tmcs',
            'fi-imatra-city.noc.ticketmaster.corp',
            'ie-olympiatheatre.netops.tmcs',
            'remote_VicarStDublin.noc.ticketmaster.corp',
            'dk-sonderborghus.netops.tmcs',
            'es-atento-callcentre-2.netops.tmcs',
            'remote_AikenPromotionsDublin.noc.ticketmaster.corp',
            'uk2-middlesbroughfc.noc.ticketmaster.corp',
            'no-oslofilharmoniske.noc.ticketmaster.corp',
            'uk2-manutd-hosted-testing.netops.tmcs'
            ]

            for values in api_output:
                for entries in values:
                    if entries["Caption"] in host_staging:
                        host_list.append(entries["IP_Address"])
                        hostvars = {
                            entries["IP_Address"]: {
                                "country": entries["Country"],
                                "city": entries["City"],
                                "ansible_hostname": entries["Caption"],
                                "ansible_host": entries["IP_Address"],
                                "os_version": entries["IOSVersion"],
                                "router_model": entries["MachineType"]
                            }
                        }
                        host_var.update(hostvars)

                inventory = {
                    "staging": {
                        "hosts": host_list,
                        "vars": {}
                    },
                    "_meta": {
                        "hostvars": host_var
                    }
                }
                return inventory
        else:
            print 'Something went wrong with the GET request/respons'
            sys.exit(1)

    @staticmethod
    def empty_inventory():
        '''
        Empty inventory for testing.
        '''
        return {"_meta": {"hostvars": {}}}

    def read_cli_args(self):
        '''
        Read the command line args passed to the script.
        '''
        parser = argparse.ArgumentParser()
        parser.add_argument('--list', action='store_true')
        parser.add_argument('--host', action='store')
        self.args = parser.parse_args()

# Get the inventory.
DynamicInventory()
