#!/usr/bin/python
import re

class FilterModule(object):
    def filters(self):
        return { 'os_var_filter': self.os_var_filter }

    def os_var_filter(self, stdout):
        match = re.search(r'\d\d\.\d', stdout, re.M)
        os_version = match.group()
        return float(os_version)
