#!/usr/bin/python
import re

class FilterModule(object):
    def filters(self):
        return { 'l3_if_up': self.l3_if_up }

    def l3_if_up(self, stdout):
        l3_if_upup = list()
        for i in stdout.split():
            if 'Vlan' in i or 'Ethernet' in i or `Tunnel` in i:
                l3_if_upup.append(i)
        return l3_if_upup
